package com.example.ravishkumar.googleplacesdemo.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.ravishkumar.googleplacesdemo.Activity.MapActivity;
import com.example.ravishkumar.googleplacesdemo.Util.AppConstant;
import com.example.ravishkumar.googleplacesdemo.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Aniket on 8/31/2016.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
private ArrayList<String> mlistPhotos;
    private LayoutInflater layoutInflater;
    private Activity mActivity;

    public  RecyclerViewAdapter(ArrayList<String> mlistPhotos, Activity mActivity)
    {
        this.mlistPhotos=mlistPhotos;
        this.mActivity=mActivity;
        layoutInflater=LayoutInflater.from(mActivity);
    }


    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = layoutInflater.inflate(R.layout.view_recyler_image, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapter.ViewHolder holder, final int position)
    {
        Picasso.with(mActivity).load(AppConstant.PHOTOURL+mlistPhotos.get(position)+AppConstant.PHOTOURL_APIKEY_URL+AppConstant.API_KEY).into(holder.IvPhotos);

        holder.IvPhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(mActivity instanceof MapActivity)
                {
                    ((MapActivity)mActivity).loadImage(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mlistPhotos.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        ImageView IvPhotos;

        public ViewHolder(View view)
        {
            super(view);
            IvPhotos= (ImageView) view.findViewById(R.id.iv_photos);

        }
    }


}

package com.example.ravishkumar.googleplacesdemo.Activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.example.ravishkumar.googleplacesdemo.Util.AppConstant;
import com.example.ravishkumar.googleplacesdemo.Adapter.FullImageViewAdapter;
import com.example.ravishkumar.googleplacesdemo.R;

import java.util.ArrayList;

/**
 * Created by Aniket on 8/31/2016.
 */
public class FullImageActivity extends AppCompatActivity
{

    private ArrayList<String> mlistImage;

    private ViewPager mViewPager;
    private int Position;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_full_image);
        mlistImage=getIntent().getStringArrayListExtra(AppConstant.PHOTOLIST);
        Position=getIntent().getIntExtra(AppConstant.POSITION,0);

        // Initalze View
        initView();



        // set up data
        FullImageViewAdapter mFullImageViewAdapter=new FullImageViewAdapter(FullImageActivity.this,getSupportFragmentManager(),mlistImage);
        mViewPager.setAdapter(mFullImageViewAdapter);
        mViewPager.setCurrentItem(Position);

    }

    private void initView()
    {
        mViewPager=(ViewPager)findViewById(R.id.pager);
    }


}

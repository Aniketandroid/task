package com.example.ravishkumar.googleplacesdemo.Model;

/**
 * Created by Aniket on 7/15/2016.
 */
public class GooglePlacesModel {
    private String address;
    private String place_id;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }
}

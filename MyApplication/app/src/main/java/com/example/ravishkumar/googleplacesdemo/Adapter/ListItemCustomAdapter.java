package com.example.ravishkumar.googleplacesdemo.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.ravishkumar.googleplacesdemo.Model.GooglePlacesModel;
import com.example.ravishkumar.googleplacesdemo.R;

import java.util.List;

/**
 * Created by Aniket on 7/15/2016.
 */
public class ListItemCustomAdapter extends BaseAdapter {

List<GooglePlacesModel> mList;
    private Context mContext;
    public ListItemCustomAdapter(Activity mActivity, List<GooglePlacesModel> list)
    {
        mContext=mActivity;
        this.mList=list;
    }
    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        if(convertView==null)
        {
            convertView= LayoutInflater.from(mContext).inflate(R.layout.listviewitem,null);
        }

        TextView mTv=(TextView)convertView.findViewById(R.id.tv_address);
        mTv.setText(mList.get(position).getAddress());
        convertView.setTag(mList.get(position));
        return convertView;
    }
}

package com.example.ravishkumar.googleplacesdemo.Adapter;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.ravishkumar.googleplacesdemo.Fragment.FullImageFragment;

import java.util.ArrayList;

/**
 * Created by Aniket on 8/31/2016.
 */
public class FullImageViewAdapter extends FragmentStatePagerAdapter
{
    private ArrayList<String> mlistImage;


    public FullImageViewAdapter(Activity mActivity, FragmentManager mFragmentManager, ArrayList<String> mlistImage){

            super(mFragmentManager);
        this. mlistImage=mlistImage;

    }
    {

    }
    @Override
    public Fragment getItem(int position) {
        return FullImageFragment.getInstance(mlistImage.get(position));
    }

    @Override
    public int getCount() {
        return mlistImage.size();
    }
}

package com.example.ravishkumar.googleplacesdemo.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.ravishkumar.googleplacesdemo.Util.AppConstant;
import com.example.ravishkumar.googleplacesdemo.Util.DeviceManager;
import com.example.ravishkumar.googleplacesdemo.R;
import com.example.ravishkumar.googleplacesdemo.Adapter.RecyclerViewAdapter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

/**
 * Created by Aniket on 8/30/2016.
 */
public class MapActivity extends AppCompatActivity {
    private Double mLatitude, mLongitude;
    private RecyclerView mRecyclerView;
    private ArrayList<String> mlistPhotos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_layout);

        // set Toolbar
        setBar();
//Initalize View
        initView();



        //set Up map
        initMap();



        if (mlistPhotos != null && mlistPhotos.size() > 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
            RecyclerViewAdapter mRecyclerViewAdapter = new RecyclerViewAdapter(mlistPhotos, MapActivity.this);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(MapActivity.this, LinearLayoutManager.HORIZONTAL, false));
            mRecyclerView.setAdapter(mRecyclerViewAdapter);
        }


    }

    public void loadImage(int position) {
        if (DeviceManager.isNetworkAvailable(MapActivity.this)) {
            Intent mIntent = new Intent(MapActivity.this, FullImageActivity.class);
            mIntent.putStringArrayListExtra(AppConstant.PHOTOLIST, mlistPhotos);
            mIntent.putExtra(AppConstant.POSITION, position);
            startActivity(mIntent);
        } else
            Toast.makeText(MapActivity.this, R.string.no_internt_connection, Toast.LENGTH_LONG).show();

    }

    private void initView() {
        mLatitude = Double.parseDouble(getIntent().getStringExtra(AppConstant.LATITUDE));
        mLongitude = Double.parseDouble(getIntent().getStringExtra(AppConstant.LONGITUDE));
        mlistPhotos = getIntent().getStringArrayListExtra(AppConstant.PHOTOLIST);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
    }

    private void setBar() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initMap()
    {

        GoogleMap mGoogleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        mGoogleMap.getUiSettings().setCompassEnabled(false);
        mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);



        LatLng sydney = new LatLng(mLatitude,mLongitude);
        mGoogleMap.addMarker(new MarkerOptions().position(sydney).title(""));

        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLatitude, mLongitude), 13), 1500, null);
    }
}

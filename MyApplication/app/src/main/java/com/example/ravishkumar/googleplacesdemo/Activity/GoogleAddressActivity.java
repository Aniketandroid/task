package com.example.ravishkumar.googleplacesdemo.Activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.transition.Explode;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ravishkumar.googleplacesdemo.Util.AppConstant;
import com.example.ravishkumar.googleplacesdemo.Util.DeviceManager;
import com.example.ravishkumar.googleplacesdemo.Model.GooglePlacesModel;
import com.example.ravishkumar.googleplacesdemo.Adapter.ListItemCustomAdapter;
import com.example.ravishkumar.googleplacesdemo.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class GoogleAddressActivity extends AppCompatActivity {


    private EditText mEtPlaces;
    private ListView mLvPlaces;

    private static final String LOG_TAG = "Google Places Autocomplete";



    private PlaceAsynTask mPlaceAsynTask;
    private ArrayList<String> PhotosReferenceList;
    List<GooglePlacesModel> resultList = null;
    ListItemCustomAdapter mListItemCustomAdapter;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // set Toolbar
        setBar();
//Initalize View
        initView();


      // load store data
        resultList=loadSharedPreferencesLogList(GoogleAddressActivity.this);

         mListItemCustomAdapter=  new ListItemCustomAdapter(GoogleAddressActivity.this,resultList);


        mLvPlaces.setAdapter(mListItemCustomAdapter);


        // Text Change Listener
        mEtPlaces.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0) {
                    if (DeviceManager.isNetworkAvailable(GoogleAddressActivity.this)) {
                        if (mPlaceAsynTask != null) {
                            mPlaceAsynTask.cancel(true);
                            mPlaceAsynTask = null;
                        }

                        if (mPlaceAsynTask == null) {
                            mPlaceAsynTask = new PlaceAsynTask(s.toString());
                        }
                        mPlaceAsynTask.execute();
                    } else {
                        Toast.makeText(GoogleAddressActivity.this, R.string.no_internt_connection, Toast.LENGTH_LONG).show();
                    }
                } else {
                    resultList=loadSharedPreferencesLogList(GoogleAddressActivity.this);
                    mListItemCustomAdapter=  new ListItemCustomAdapter(GoogleAddressActivity.this,resultList);
                    mLvPlaces.setAdapter(mListItemCustomAdapter);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        // On item Click listener
        mLvPlaces.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                DeviceManager.hide(GoogleAddressActivity.this,mEtPlaces);
                if(DeviceManager.isNetworkAvailable(GoogleAddressActivity.this)) {

                    GooglePlacesModel mGooglePlacesModel= (GooglePlacesModel) view.getTag();
                    saveSharedPreferencesLogList(GoogleAddressActivity.this,mGooglePlacesModel);
                new PlaceIDAsynTask(mGooglePlacesModel.getPlace_id()).execute();
            }
            else
            {
                Toast.makeText(GoogleAddressActivity.this, R.string.no_internt_connection,Toast.LENGTH_LONG).show();
            }
            }
        });

    }


    /**
     * used to get the autocomplete places
     */
    private class PlaceAsynTask extends AsyncTask<Void, Void, Void> {

        String input;


        PlaceAsynTask(String input) {
            this.input = input;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            HttpURLConnection conn = null;

            StringBuilder jsonResults = new StringBuilder();

            try {

                StringBuilder sb = new StringBuilder(AppConstant.PLACES_API_BASE + AppConstant.TYPE_AUTOCOMPLETE + AppConstant.OUT_JSON);

                sb.append("?key=" + AppConstant.API_KEY);

                sb.append("&components=country:in");

                sb.append("&input=" + URLEncoder.encode(input, "utf8"));

                URL url = new URL(sb.toString());

                conn = (HttpURLConnection) url.openConnection();

                InputStreamReader in = new InputStreamReader(conn.getInputStream());

                // Load the results into a StringBuilder

                int read;

                char[] buff = new char[1024];

                while ((read = in.read(buff)) != -1) {

                    jsonResults.append(buff, 0, read);

                }

            } catch (MalformedURLException e) {

            } catch (IOException e) {


            } finally {

                if (conn != null) {

                    conn.disconnect();

                }
            }


            try {

                // Create a JSON object hierarchy from the results

                JSONObject jsonObj = new JSONObject(jsonResults.toString());

                JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");


                // Extract the Place descriptions from the results

                if(predsJsonArray.length()>0) {
                    resultList = new ArrayList<GooglePlacesModel>(predsJsonArray.length());

                    for (int i = 0; i < predsJsonArray.length(); i++) {
                        GooglePlacesModel mGooglePlacesModel = new GooglePlacesModel();

                        mGooglePlacesModel.setAddress(predsJsonArray.getJSONObject(i).getString("description"));
                        mGooglePlacesModel.setPlace_id(predsJsonArray.getJSONObject(i).getString("place_id"));
                        resultList.add(mGooglePlacesModel);

                    }
                }


            } catch (JSONException e) {

                Log.e("Cannot process JSON results", e.getMessage());

            }

return null;
        }


        @Override
        protected void onPostExecute(Void Void) {
            super.onPostExecute(Void);

            if(resultList!=null) {

                mListItemCustomAdapter=  new ListItemCustomAdapter(GoogleAddressActivity.this,resultList);
                mLvPlaces.setAdapter(mListItemCustomAdapter);            }

        }
    }


    /**
     * USed to get the place detail include latlong and places
     */
    private class PlaceIDAsynTask extends AsyncTask<Void, Void, Void> {

        String input;
        String  latitude,longitude;


        PlaceIDAsynTask(String input) {
            this.input = input;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            HttpURLConnection conn = null;

            StringBuilder jsonResults = new StringBuilder();

            try {

                StringBuilder sb = new StringBuilder(AppConstant.PLACES_API_BASE +AppConstant.PLACES_DETAILS + AppConstant.OUT_JSON);

                sb.append("?key=" + AppConstant.API_KEY);




                sb.append("&placeid=" + URLEncoder.encode(input, "utf8"));

                URL url = new URL(sb.toString());

                conn = (HttpURLConnection) url.openConnection();

                InputStreamReader in = new InputStreamReader(conn.getInputStream());

                // Load the results into a StringBuilder

                int read;

                char[] buff = new char[1024];

                while ((read = in.read(buff)) != -1) {

                    jsonResults.append(buff, 0, read);

                }

            } catch (MalformedURLException e) {

            } catch (IOException e) {


            } finally {

                if (conn != null) {

                    conn.disconnect();

                }
            }


            try {

                // Create a JSON object hierarchy from the results

                JSONObject jsonObj = new JSONObject(jsonResults.toString());

                JSONObject mJsonResult=jsonObj.getJSONObject("result");

              JSONObject mJsonGeomatrci=mJsonResult.getJSONObject("geometry");

                JSONObject mjSOnLocation=mJsonGeomatrci.getJSONObject("location");


                JSONArray JsonPhotos=mJsonResult.getJSONArray("photos");


                PhotosReferenceList=new ArrayList<>();


                if(JsonPhotos!=null&&JsonPhotos.length()>0)
                {
                    for(int i=0;i<JsonPhotos.length();i++)
                    {
                        JSONObject mJsonObject=JsonPhotos.getJSONObject(i);
                        PhotosReferenceList.add(mJsonObject.getString("photo_reference"));
                    }
                }
                latitude=mjSOnLocation.getString("lat");
                longitude=mjSOnLocation.getString("lng");



            } catch (JSONException e) {

                Log.e("Cannot process JSON results", e.getMessage());

            }

            return null;
        }


        @Override
        protected void onPostExecute(Void Void) {
            super.onPostExecute(Void);
if(latitude!=null) {

    Intent mIntent=new Intent(GoogleAddressActivity.this,MapActivity.class);
    mIntent.putStringArrayListExtra(AppConstant.PHOTOLIST,PhotosReferenceList);
    mIntent.putExtra(AppConstant.LATITUDE,latitude);
    mIntent.putExtra(AppConstant.LONGITUDE,longitude);
    startActivity(mIntent);
}

        }
    }

    /**
     * used to Store the previous Seached Result
     * @param context
     * @param mGooglePlacesModel
     */
    public static void saveSharedPreferencesLogList(Context context, GooglePlacesModel mGooglePlacesModel) {


        List<GooglePlacesModel> mlist=loadSharedPreferencesLogList(context);

        for(int i=0;i<mlist.size();i++)
        {
            if(mlist.get(i).getPlace_id().equalsIgnoreCase(mGooglePlacesModel.getPlace_id()))
            {
                mlist.remove(i);

            }

        }

        mlist.add(0,mGooglePlacesModel);
        SharedPreferences mPrefs = context.getSharedPreferences(AppConstant.SAVED_ADDRESS_LIST, context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(mlist);
        prefsEditor.putString("myList", json);
        prefsEditor.commit();
    }


    /**
     * used to fetch the stored result if any
     * @param context
     * @return
     */
    public static List<GooglePlacesModel> loadSharedPreferencesLogList(Context context) {
        List<GooglePlacesModel> mlist = new ArrayList<GooglePlacesModel>();
        SharedPreferences mPrefs = context.getSharedPreferences(AppConstant.SAVED_ADDRESS_LIST, context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString("myList", "");
        if (json.isEmpty()) {
            mlist = new ArrayList<GooglePlacesModel>();
        } else {
           Type type = new TypeToken<List<GooglePlacesModel>>() {
            }.getType();
            mlist = gson.fromJson(json, type);
        }
        return mlist;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                break;
        }


        return super.onOptionsItemSelected(item);
    }

    private  void setBar()
    {
        Toolbar mToolbar=(Toolbar)findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initView()
    {
        mEtPlaces = (EditText) findViewById(R.id.et_places);

        mLvPlaces = (ListView) findViewById(R.id.lv_places);
    }
}

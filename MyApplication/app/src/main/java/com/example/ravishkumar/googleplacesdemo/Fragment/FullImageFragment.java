package com.example.ravishkumar.googleplacesdemo.Fragment;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.ravishkumar.googleplacesdemo.R;
import com.example.ravishkumar.googleplacesdemo.Util.AppConstant;
import com.example.ravishkumar.googleplacesdemo.Util.DeviceManager;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;

/**
 * Created by Aniket on 8/31/2016.
 */
public class FullImageFragment extends Fragment
{

    private ImageView mIvFullImage;
    private Button mButtonDownload;
     String ImageUrl="";
     FullImageFragment mFullImageFragment;

    public static FullImageFragment getInstance(String ImageUrl){
        FullImageFragment mFullImageFragment=new FullImageFragment();
        mFullImageFragment.ImageUrl=ImageUrl;
return mFullImageFragment;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View mView=inflater.inflate(R.layout.layout_full_image,container,false);


// initalize View

        mIvFullImage=(ImageView)mView.findViewById(R.id.iv_full_image);
        mButtonDownload=(Button)mView.findViewById(R.id.btn_download);

        // Image Url

final  String Url= AppConstant.PHOTOURL+ImageUrl+AppConstant.PHOTOURL_APIKEY_URL+AppConstant.API_KEY;


        // load image on view
        Picasso.with(getActivity()).load(Url).into(mIvFullImage);



        // Download Button listener
        mButtonDownload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(DeviceManager.isNetworkAvailable(getActivity())) {
                    if (isExternalStorageAvailable()) {


                        // Asyntask to Download image
                        new DownloadAsynTask(Url).execute();



                    } else {
                        Toast.makeText(getActivity(), R.string.Sd_card_not_available, Toast.LENGTH_LONG).show();
                    }
                }
                else
                Toast.makeText(getActivity(),R.string.no_internt_connection,Toast.LENGTH_LONG).show();
            }
        });

        return mView;
    }


    /**
     *  check wheather the sd Card is available or not
     * @return
     */
    private boolean isExternalStorageAvailable() {

        String state = Environment.getExternalStorageState();
        boolean mExternalStorageAvailable = false;
        boolean mExternalStorageWriteable = false;

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            // Something else is wrong. It may be one of many other states, but
            // all we need
            // to know is we can neither read nor write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }

        if (mExternalStorageAvailable == true
                && mExternalStorageWriteable == true) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * ASyntask to Download Image
     * */
    private class DownloadAsynTask extends AsyncTask<Void,Void,Void>{

private String Url;
        private Exception exception;
        DownloadAsynTask(String Url)
        {
            this.Url=Url;
        }
        ProgressDialog mProgressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
             mProgressDialog=new ProgressDialog(getActivity());
            mProgressDialog.setMessage(getString(R.string.Downloading_image));
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String name = new Date().toString() + ".jpg";
            try {


                downloadImagesToSdCard(Url,name);


            } catch (Exception e) {
                exception=e;
                e.printStackTrace();
            }


            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(mProgressDialog!=null&&mProgressDialog.isShowing())
            {
                mProgressDialog.dismiss();
            }
            if(exception!=null)
            {
                Toast.makeText(getActivity(),exception.getMessage(),Toast.LENGTH_LONG).show();
            }
            else
            {
                Toast.makeText(getActivity(), R.string.downloading_message,Toast.LENGTH_LONG).show();

            }

        }
    }


    /**
     * Used to Store the Image on SD card
     * @param downloadUrl
     * @param imageName
     */
    private void downloadImagesToSdCard(String downloadUrl,String imageName)
    {
        try
        {
            URL url = new URL(downloadUrl);
                        /* making a directory in sdcard */
            String sdCard=Environment.getExternalStorageDirectory().toString();
            File myDir = new File(sdCard,"Map");

                        /*  if specified not exist create new */
            if(!myDir.exists())
            {
                myDir.mkdir();
            }

                        /* checks the file and if it already exist delete */
            String fname = imageName;
            File file = new File (myDir, fname);
            if (file.exists ())
                file.delete ();

                             /* Open a connection */
            URLConnection ucon = url.openConnection();
            InputStream inputStream = null;
            HttpURLConnection httpConn = (HttpURLConnection)ucon;
            httpConn.setRequestMethod("GET");
            httpConn.connect();

            if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK)
            {
                inputStream = httpConn.getInputStream();
            }

            FileOutputStream fos = new FileOutputStream(file);
            int totalSize = httpConn.getContentLength();
            int downloadedSize = 0;
            byte[] buffer = new byte[1024];
            int bufferLength = 0;
            while ( (bufferLength = inputStream.read(buffer)) >0 )
            {
                fos.write(buffer, 0, bufferLength);
                downloadedSize += bufferLength;
            }

            fos.close();

            MediaScannerConnection.scanFile(getActivity(),
                    new String[] { file.toString() }, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {

                        }
                    });





        }
        catch(IOException io)
        {
            io.printStackTrace();
            try {
                throw   new Exception(" Error In Downloading");
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            try {
                throw   new Exception(" Error In Downloading");
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }
}

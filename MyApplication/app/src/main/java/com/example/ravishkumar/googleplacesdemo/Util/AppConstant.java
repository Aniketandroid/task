package com.example.ravishkumar.googleplacesdemo.Util;

/**
 * Created by Aniket on 8/31/2016.
 */
public class AppConstant
{
    public static final String SAVED_ADDRESS_LIST="Address List";

    public static final String PHOTOURL="https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=";
    public static final String PHOTOURL_APIKEY_URL="&sensor=false&key=";
    public static final String API_KEY = "AIzaSyC10djfwd6ZJseLd82n4x5P_OPZLvw4r7E";

    public static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    public static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    public static final String PLACES_DETAILS = "/details";


    public static final String OUT_JSON = "/json";


    public static final String LATITUDE = "Latitude";
    public static final String LONGITUDE = "Longitude";
    public static final String PHOTOLIST = "PhotoList";
    public static final String POSITION = "Position";




}

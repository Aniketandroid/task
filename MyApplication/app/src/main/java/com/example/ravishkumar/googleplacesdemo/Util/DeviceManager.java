package com.example.ravishkumar.googleplacesdemo.Util;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * Created by Aniket                                                 z  on 8/31/2016.
 */
public class DeviceManager {

    /**
     * Check Network Connection
     *
     * @return
     */
    public static boolean isNetworkAvailable(Context mContext)
    {
        //Get Application Context
        Context appContext = mContext;

        //Get ConnectivityManager
        ConnectivityManager connectivityManager = (ConnectivityManager)appContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        //Get NetworkInfo
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        return ((networkInfo == null || !networkInfo.isConnected()) ? false : true);
    }


    public static void hide(Activity activity, EditText mEditText){
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromInputMethod(mEditText.getWindowToken(), 0); // hide
    }

}
